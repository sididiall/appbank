<%@include file="header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<H2 class="alert alert-secondary">Virement</H2>

<form action="${pageContext.request.contextPath}/Transfer" method="post">
	<input type="hidden" value="${pageContext.request.contextPath}" name="path"/>
	
	<c:if test="${not empty newTransaction}">
       	<div id="transact-success" class="alert alert-success" role="alert">
		  	<p class="mb-0">Le virement à bien été exécuté</p>
		  	<hr>
			<span>Date : ${newTransaction.getDate()} </span><br /> 
			<span>Montant : ${newTransaction.getAmount()} </span><br /> 
			<span>Libellé: ${newTransaction.getWording()} </span><br /> 
		</div>
   	</c:if>
	
	<div class="form-control">
	
		<div class="row">
			<div class="col">
				<label for="accountCS">Compte Emetteur</label>	
				<select class="form-control" id="accountCS" name="accountCS">
					<c:set var="count" value="0" />
					<c:forEach items="${accountCS}" var="accountCS">
						<c:set var="count" value="${count + 1}" scope="page"/>
						<option value="<c:out value="${count}"/>">
							${accountCS.wording} - ${accountCS.number} - solde: ${hashbalanceA[accountCS.accountId]} €
						</option>
					</c:forEach>
				</select>
				<br /> 
			</div>
			<div class="col">
				<label for="accountCR">Compte Récepteur</label>
				<select class="form-control" id="accountCR" name="accountCR">
				<c:set var="count" value="0" />
					<c:forEach items="${accountCR}" var="accountCR">
						<c:set var="count" value="${count + 1}" scope="page"/>
						<option value="<c:out value="${count}"/>">
							${accountCR.wording} - ${accountCR.number}
						</option>
					</c:forEach>
				</select> 
				<br />  
			</div>
		</div>
		
	</div>
	<br /> 
	
	<label for="wording">Libellé</label>
	<input class="form-control" id="wording" type="text" name="wording" /> 
	<c:if test="${not empty errorW}">
       	<div class="alert alert-danger" role="alert">
	  		${errorW}
		</div>
    </c:if>
	<br /> 
	<label for=amount>Montant</label>
	<input class="form-control" id="amount" type="number" name="amount" /> 
	<c:if test="${not empty errorA}">
       	<div class="alert alert-danger" role="alert">
	  		${errorA}
		</div>
    </c:if>
	
	<c:if test="${not empty error}">
       	<div class="alert alert-danger" role="alert">
	  		${errorform}
		</div>
    </c:if>
	<br /> 
	<input type="submit" name="submit" />
	<br /> <br /> 
</form>

<%@include file="footer.jsp"%>