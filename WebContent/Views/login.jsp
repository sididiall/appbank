<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.List"%>
<%@page import="fr.ynovbank.diall.model.Client"%>
<%@page import="fr.ynovbank.diall.dao.ClientManager"%>
<%@page import="java.util.ArrayList"%>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="fr.ynovbank.diall.i18n.text" />
<!DOCTYPE html>
<html lang="${language}">
 <head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/Assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Assets/css/reset.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Assets/css/all.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700" rel="xstylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/Assets/js/jquery-3.2.1.min.js"></script>
<title>APP BANK - Connexion</title>
</head>
    <body class="page-content">
    
	<div class="header">Ma Banque En ligne</div>
	
    <div class="header-container">
    	<c:if test="${not empty message}">
        	<div class="logout-section alert alert-warning alert-dismissible fade show">
		 		<strong>${message} </strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    				<span aria-hidden="true">&times;</span>
				</button>
			</div>
        </c:if>
        
        <c:if test="${not empty ExpiredMessage}">
	        <div class="alert alert-primary logout-section alert alert-warning alert-dismissible fade show" role="alert">
			  	<h4 class="alert-heading">Session Expiré</h4>
			  	<p>${ExpiredMessage}</p>
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    				<span aria-hidden="true">&times;</span>
				</button>
			</div>
        </c:if>
		
		<div class="langage">
			<form>
	            <select  class="form-control" id="language" name="language" onchange="submit()">
	                <option value="fr" ${language == 'fr' ? 'selected' : ''}>Français</option>
	                <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
	            </select>
	        </form>
		</div>
    </div>
	<form class="login" action="${pageContext.request.contextPath}/Login" method="post">	
        <label for="username"><fmt:message key="login.label.username" />:</label>
        <input  class="form-control" type="text" id="username" name="username" />
        <br>
        <label for="password"><fmt:message key="login.label.password" />:</label>
        <input  class="form-control" type="password" id="passwd" name="passwd"  >
        <br>
        
        <c:if test="${not empty error}">
        	<div class="alert alert-danger" role="alert">
		  		${error}
			</div>
        </c:if>
        <fmt:message key="login.button.submit" var="buttonValue" />
        <input type="submit" name="submit" >
    </form>
    
	<script src="${pageContext.request.contextPath}/Assets/js/index.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </body>
</html>