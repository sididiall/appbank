<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jsp"%>

<div class="page-heading">Comptes Disponibles :</div>
<div class="table-content">
	<table class="table table-hover">
		<thead >
			<tr>
				<th>Nom de Compte</th>
				<th>Numéro de Compte</th>
				<th>Solde</th>
				<th>Details</th>
			</tr>
		</thead>
		<tbody>
			<c:set var="count" value="-1" />
			<c:forEach items="${clientlogin.accounts}" var="accounts">
			
			<tr>
				<td>${accounts.wording}</td>
				<td>${accounts.number}</td>
				<td>
					${hashbalanceA[accounts.accountId]} €
				</td>
				<td>
					<c:set var="count" value="${count + 1}" scope="page"/>
					<span class="details">
						<a id="accountidx" href="${pageContext.request.contextPath}/Transactions/acountidx=${count}">
							<i class="fa fa-eye" aria-hidden="true"></i>
						</a>
					</span>
				</td>
			</tr>
			</c:forEach>
			<tr>
				<td colspan="4">Solde disponible ${balanceAvailable} €</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="page-footer">
	<div class="add-dropdown dropdown">
  		<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    	<i class="fa fa-plus" aria-hidden="true"></i>
	  	</button>
	  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="${pageContext.request.contextPath}/Transfer"><i class="fa fa-exchange" aria-hidden="true"></i>Effectuer un virement</a>
		    <a class="dropdown-item" href="${pageContext.request.contextPath}/CreateAccount"><i class="fa fa-book" aria-hidden="true"></i>Créer un compte</a>
	 	</div>
	</div>
</div>
<%@include file="footer.jsp"%>