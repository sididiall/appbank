<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jsp"%>
<div class="alert alert-primary"><p>${accountByIdx.getWording()} </p></div>

<div class="alert alert-secondary"><p><p>Listes de transactions du compte ${accountByIdx.getNumber()}</div>

<input type="hidden" value="${pageContext.request.contextPath}" name="path"/>
<input type="hidden" value="${acountidxint}" name="index"/>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Libellé</th>
			<th>Date</th>
			<th>Montant</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${Transactions}" var="transactions">
		<tr>
			<td>${transactions.wording}</td>
			<td>${transactions.date}</td>
			<td>${transactions.amount} €</td>
		</tr>
		</c:forEach>
		<c:if test="${not empty emptyTransact}">
			<tr>
				<td colspan="3">${emptyTransact}
			</tr>
    	</c:if>
	</tbody>
</table>


<%@include file="footer.jsp"%>