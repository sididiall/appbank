<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.List"%>
<%@page import="fr.ynovbank.diall.model.Client"%>
<%@page import="fr.ynovbank.diall.dao.ClientManager"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Assets/css/reset.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/Assets/css/all.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/Assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700" rel="stylesheet">
<script src="${pageContext.request.contextPath}/webjars/jquery/2.1.4/jquery.js"></script>
<title>APP BANK</title>
</head>
<body class="page-content">
	<div class="header">
		<a href="${pageContext.request.contextPath}/Accounts" title="Acceuil">
			<span class="home"><i class="fa fa-home fa-3" aria-hidden="true"></i></span>
		</a>
		<div style="text-align: center">
			<span>Ma Banque En ligne</span><br /> 
			<span>${clientlogin.getFirstname()} ${clientlogin.getName()}</span>
			<span> [Client: ${clientlogin.getClientNumber()}]</span>
		</div>
		
		<div class="client-dropdown dropdown">
	  		<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    	<i class="fa fa-user-o" aria-hidden="true"></i>
		  	</button>
		  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			    <a class="dropdown-item" href="${pageContext.request.contextPath}/Client/clientId=${clientlogin.getClientId()}" title="Paramètre"><i class="fa fa-wrench" aria-hidden="true"></i>Paramètre</a>
			    <a class="dropdown-item" href="${pageContext.request.contextPath}/Logout"  title="Deconnexion"><i class="fa fa-sign-out" aria-hidden="true"></i>Déconnexion</a>
		 	</div>
		</div>
	</div>
	
	<div class="content">
	<div class=" body-container container">