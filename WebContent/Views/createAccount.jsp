<%@include file="header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<H2 class="alert alert-secondary">Nouveau Compte</H2>

<form action="${pageContext.request.contextPath}/CreateAccount" method="post">
	
	<input type="hidden" value="${pageContext.request.contextPath}" name="path"/>
	
	<c:if test="${not empty newAccount}">
       	<div id="account-success" class="alert alert-success" role="alert">
		  	<p class="mb-0">Le compte à bien été créer</p>
		  	<hr>
			<span>Numéro de compte : ${newAccount.getNumber()} </span><br /> 
			<span>Libellé: ${newAccount.getWording()} </span><br /> 
		</div>
   	</c:if>
   	
	<div class="form-control">
		<div class="alert alert-primary" role="alert">Libellé du compte	</div>
		<div class="row">
			<div class="col">
				<label for="wording-prefix">Préfixe du libellé</label><br /> 
				<select class="form-control" id="wording-prefix" type="text" name="wording-prefix">
					<option value="Compte">Compte</option>
					<option value="Livret">Livret</option>
				</select> 
			</div>	
			<div class="col">
				<label for="wording-suffix">Suffixe du libellé</label><br /> 
				<input class="form-control"  id="wording-suffix" type="text" name="wording-suffix" /> 
			</div>	
		</div>
		<c:if test="${not empty error}">
        	<div class="alert alert-danger" role="alert">
		  		${error}
			</div>
        </c:if>
	</div>
	<input type="submit" name="submit" />
	<br /> 
</form>

<%@include file="footer.jsp"%>