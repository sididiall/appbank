<%@include file="header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h2 class="alert alert-secondary" style="margin-top:60px">Modification mot de passe</h2>

<form action="${pageContext.request.contextPath}/Client/clientId=${clientlogin.getClientId()}" method="post">

	<input type="hidden" value="${pageContext.request.contextPath}" name="path"/>
	
	<c:if test="${success == true}">
       	<div id="password-success" class="alert alert-success" role="alert">
		  	<p class="mb-0">Le mot de passe à bien été modifier</p>
		</div>
   	</c:if>

	<div class="form-control">
		<label for="old-passwd">Ancien mot de passe</label>	
		<input class="form-control" id="old-passwd" type="password" name="old-passwd" required /> 
		<small id="emailHelp" class="form-text text-muted">Vous devez saisir votre ancien mot de passe</small>
	</div>
	<br /> 
	<div class="form-control">
		<label for="passwd">Nouveau mot de passe</label>	
		<input class="form-control" id="passwd" type="password" name="passwd" required/> 
		<small id="emailHelp" class="form-text text-muted">Vous devez saisir un mot de passe avec 1 majuscule, 1 caractère spécial et au moins 5 lettres</small>
	</div>
	<br /> 
	<div class="form-control">
		<label for="passwd-confirm">Vérification nouveau mot de passe</label>	
		<input class="form-control" id="passwd-confirm" type="password" name="passwd-confirm" required/>
	</div>
	<c:if test="${not empty error}">
        	<div class="alert alert-danger" role="alert">
		  		${error}
			</div>
    </c:if>
	<c:if test="${not empty errorform}">
       	<div class="alert alert-danger" role="alert">
	  		${errorform}
		</div>
    </c:if>
	
	<input type="submit" name="submit" />
</form>

<%@include file="footer.jsp"%>