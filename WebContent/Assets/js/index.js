$(function() {

	// Gestion Validation
	// Transaction
	var successDivTrans = $('#transact-success');
	var contextPath = $('input[name="path"]').val();
	var url = contextPath + "/Accounts";

	if (successDivTrans.length > 0) {
		setTimeout(function() {
			window.location = url
		}, 5000);
	}

	// Account
	var successDivAcc = $('#account-success');
	var contextPath = $('input[name="path"]').val();
	var url = contextPath + "/Accounts";

	if (successDivAcc.length > 0) {
		setTimeout(function() {
			window.location = url
		}, 5000);
	}

	// Password
	var successDivAcc = $('#password-success');
	var contextPath = $('input[name="path"]').val();
	var url = contextPath + "/Accounts";

	if (successDivAcc.length > 0) {
		setTimeout(function() {
			window.location = url
		}, 5000);
	}

	// Pagination

	var contextPath = $('input[name="path"]').val();
	var index = $('input[name="index"]').val();
	var transactions = $('input[name="transactions"]').val();
	var transactionsPerPage = 10;

	// retourne le plus petit entier supérieur ou égal au nombre donné.
	var pages = Math.ceil(transactions / transactionsPerPage);

	var url = contextPath + "/RestPaginate" + index;

	function changePage(page) {
		pnumber = page || 1;

		$.ajax({
			type : 'GET',
			dataType : 'json',
			url : url + '?page=' + pnumber,
			success : function(data) {
				alert(data);
			},
			error : function() {
			}
		})
	}

	// Add this in here
	changePage();

	// Fond de couleur
});
