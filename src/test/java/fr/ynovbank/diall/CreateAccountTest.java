package fr.ynovbank.diall;

import org.junit.Assert;
import org.junit.Test;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;

class CreateAccountTest {

	@Test
	public void CreateAccountwithsameWordingException() {

		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String wording = "Compte courant";
		String message = "Le libellé existe déjà pour ce client, veuillez saisir un autre";
		try {
			ClientManager.CreateAccount(id, client, wording);
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
	}
	@Test
	public void CreateAccountwithsameWordingException2() {
		
		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String wording = "COMPTE COURANT";
		String message = "Le libellé existe déjà pour ce client, veuillez saisir un autre";
		try {
			ClientManager.CreateAccount(id, client, wording);
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
	}
	
	@Test
	public void CreateAccount() {
		
		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String wording = "Compte Epargne";
		String message = "Le libellé existe déjà pour ce client, veuillez saisir un autre";
		
		Account account = null;
		try {
			account = ClientManager.CreateAccount(id, client, wording);
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
		Assert.assertNotNull(account);
	}

}
