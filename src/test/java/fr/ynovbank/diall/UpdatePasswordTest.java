package fr.ynovbank.diall;

import org.junit.Assert;
import junit.framework.TestCase;
import java.util.List;

import org.junit.Test;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Client;

public class UpdatePasswordTest {

	@Test
	public void UpdateClientPasswdTestWithLenghtException() {
		
		
		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String old = client.getPasswd();
		String passwd = "art";
		String message = "Il faut au moins 5 caractères au mot de passe";

		try {
			ClientManager.UpdateClientPassword(id, old, passwd);
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
	}

	@Test
	public void UpdateClientPasswdTestWithUppCaseException() {
		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String old = client.getPasswd();		
		String passwd = "adedefrt*";
		String message = "Il faut au moins une majuscule au mot de passe";
		try {
			ClientManager.UpdateClientPassword(id, old, passwd);
			Assert.fail("Une excpetion s'est produite");
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
	}

	@Test
	public void UpdateClientPasswdTestWithEmptyException() {

		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String old = client.getPasswd();
		String passwd = "";
		String message = "Le champs est vide il faut insérer un mot de passe";
		try {
			ClientManager.UpdateClientPassword(id, old, passwd);
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
	}

	@Test
	public void UpdateClientPasswdTestWithSpecialCharException() {

		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String old = client.getPasswd();
		String passwd = "Azdefrtd";
		String message = "Il faut au moins une caratère special au mot de passe";
		
		try {
			ClientManager.UpdateClientPassword(id, old, passwd);
		} catch (Exception e) {
			Assert.assertEquals(message, e.getMessage());
		}
	}
	
	@Test
	public void UpdateClientPasswdTest(){

		int id = 1;
		Client client = ClientManager.GetClientById(id);
		String old = client.getPasswd();
		String passwd = "Dharles*";
		String message = "Il faut au moins une caratère special au mot de passe";
		
		boolean clientafter = false ;
		try {
			clientafter = ClientManager.UpdateClientPassword(id, old, passwd);
		} catch (Exception e) {
			Assert.assertFalse(clientafter);
		}
		Assert.assertTrue(clientafter);
	}
}
