package fr.ynovbank.diall;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.persistence.TypedQuery;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.ynovbank.diall.model.Client;
import fr.ynovbank.diall.dao.ManagerFactory;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Transaction;

public class TestInit {

	// private static final
	private static Logger logger = LogManager.getLogger(TestJPA.class);

	public static void main(String[] args) {
		EntityManager em = null;
		try {
			em = ManagerFactory.getInstance().createEntityManager();
			Random randomGenerator = new Random();

			em.getTransaction().begin();
			
			
			// Client 1
			Client client = new Client();
			client.setUsername("diii");
			client.setClientNumber(randomGenerator.nextInt(10000));
			client.setPasswd("diii");
			client.setName("Diallo");
			client.setFirstname("Charles");

			// Compte 1
			Account compte = new Account();
			compte.setWording("Compte courant");
			compte.setClient(client);
			compte.setNumber(randomGenerator.nextInt(1000000000));
			List<Account> listeComptes = new ArrayList<Account>();
			listeComptes.add(compte);
			client.setAccounts(listeComptes);

			// Transaction 1
			Transaction transaction = new Transaction();
			transaction.setAccount(compte);
			transaction.setWording("Salaire");
			transaction.setDate(new Date());
			transaction.setAmount(10000);
			compte.setTransactions(new ArrayList<Transaction>());
			compte.getTransactions().add(transaction);

			// Transaction 2
			Transaction transaction2 = new Transaction();
			transaction2.setAccount(compte);
			transaction2.setWording("Loyer");
			transaction2.setDate(new Date());
			transaction2.setAmount(-5000);
			compte.getTransactions().add(transaction2);

			// Compte 2
			Account compte2 = new Account();
			compte2.setWording("Livret A");
			compte2.setClient(client);
			compte2.setNumber(randomGenerator.nextInt(1000000000));
			listeComptes.add(compte2);
			client.setAccounts(listeComptes);

			// Transaction 1
			Transaction transactionA = new Transaction();
			transactionA.setAccount(compte2);
			transactionA.setWording("Epargne");
			transactionA.setDate(new Date());
			transactionA.setAmount(25000);
			compte2.setTransactions(new ArrayList<Transaction>());
			compte2.getTransactions().add(transactionA);

			// Client 2
			Client client2 = new Client();
			client2.setUsername("sdd");
			client2.setPasswd("sdd");
			client2.setName("Diall");
			client2.setFirstname("Sidi");
			client2.setClientNumber(randomGenerator.nextInt(10000));

			// Compte 1
			Account compteC2 = new Account();
			compteC2.setWording("Compte courant");
			compteC2.setClient(client2);
			compteC2.setNumber(randomGenerator.nextInt(1000000000));
			List<Account> listeComptesC2 = new ArrayList<Account>();
			listeComptesC2.add(compteC2);
			client2.setAccounts(listeComptesC2);

			// Transaction 1
			Transaction transactionC2 = new Transaction();
			transactionC2.setAccount(compteC2);
			transactionC2.setWording("Salaire");
			transactionC2.setDate(new Date());
			transactionC2.setAmount(100000);
			compteC2.setTransactions(new ArrayList<Transaction>());
			compteC2.getTransactions().add(transactionC2);
			// Transaction 2
			Transaction transaction2C2 = new Transaction();
			transaction2C2.setAccount(compteC2);
			transaction2C2.setWording("Loyer");
			transaction2C2.setDate(new Date());
			transaction2C2.setAmount(-500);
			compteC2.getTransactions().add(transaction2C2);
			// Transaction 3
			Transaction transaction3C2 = new Transaction();
			transaction3C2.setAccount(compteC2);
			transaction3C2.setWording("BLABLACAR");
			transaction3C2.setDate(new Date());
			transaction3C2.setAmount(-10000);
			compteC2.getTransactions().add(transaction3C2);

			em.persist(client);
			em.persist(client2);
			em.getTransaction().commit();

			TypedQuery<Client> tQuery = em.createQuery("from Client", Client.class);
			List<Client> listeClients = tQuery.getResultList();

			PersistenceUtil util = Persistence.getPersistenceUtil();
			for (Client c : listeClients) {
				logger.info(c.toString());
				logger.debug("is client loaded ? " + util.isLoaded(c));
				logger.debug("is client loaded ? " + util.isLoaded(c.getClientId()));
				Account co = c.getAccounts().get(0);
				logger.debug("are translations loaded ? " + util.isLoaded(co, "transactions"));
				co.getTransactions();
				logger.debug("are translations loaded now ? " + util.isLoaded(co, "transactions"));
				for (Transaction tran : co.getTransactions()) {
					logger.info(tran.toString());
				}
				// System.out.println (c);
			}
			logger.info("Size: " + listeClients.size());

		} finally {
			em.close();
		}

	}
}
