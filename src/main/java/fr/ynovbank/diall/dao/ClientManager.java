package fr.ynovbank.diall.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;
import fr.ynovbank.diall.model.Transaction;
import net.bytebuddy.matcher.IsNamedMatcher;

public class ClientManager {

	public static Client GetClientById(int id) {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();
		Client c = em.find(Client.class, id);
		em.close();
		return c;
	}
	
	public static List<Client> GetClients() {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		TypedQuery<Client> tQuery = em.createQuery("from Client", Client.class);
		List<Client> clients = tQuery.getResultList();

		em.close();
		return clients;
	}

	public static boolean UpdateClientPassword(int id, String oldPasswd, String passwd) throws Exception {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		Client client = em.find(Client.class, id);
		
		boolean hasUppercase = !passwd.equals(passwd.toLowerCase());
		boolean isAtLeast5 = passwd.length() >= 5;
		boolean isEmpty = passwd.isEmpty();
		boolean hasSpecial = !passwd.matches("[A-Za-z0-9 ]*");
		boolean isSame = client.getPasswd().equals(passwd);
		boolean success = false;
		boolean isSameoldpasswd = oldPasswd.equals(client.getPasswd());
		
		if (client != null) {
			if (isEmpty) {
				throw new Exception("Le champs est vide il faut insérer un mot de passe");
			}
			if (!isAtLeast5) {
				throw new Exception("Il faut au moins 5 caractères au mot de passe");
			}
			if (!hasUppercase) {
				throw new Exception("Il faut au moins une majuscule au mot de passe");
			}
			if (!hasSpecial) {
				throw new Exception("Il faut au moins un caractère special au mot de passe");
			}
			if (isSame) {
				throw new Exception("Le mot de passe saisi est le même mot de passe actuel");
			}
			if (!isSameoldpasswd) {
				throw new Exception("Le mot de passe saisi ne correspond pas au même mot de passe en base");
			}
			client.setPasswd(passwd);
			em.persist(client);
			em.getTransaction().commit();
			
			success = true;
		}
		em.close();
		return success;
	}
	
	public static Account CreateAccount(int clientid, Client client, String wording) throws Exception {
		
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		Random randomGenerator = new Random();
		
		Client clientlogged = GetClientById(clientid);
		List<Account> ClientloggedAccounts = clientlogged.getAccounts();
		List<Account> Accounts = GetAccounts();

		Account newAccount = new Account();
		
		for (Account account : ClientloggedAccounts) {
			
			String accountswording = account.getWording();
			boolean isSameRegex = Pattern.compile(Pattern.quote(accountswording)).matcher(wording).find();
			if(isSameRegex){
				throw new Exception("Le compte avec le libellé choisi existe déjà pour ce client, veuillez choisir un autre");
			}
		}
		
		newAccount.setWording(wording);
		newAccount.setClient(clientlogged);
		
		newAccount.setNumber(randomGenerator.nextInt(1000000000));
		for (Account account : Accounts) {
			boolean isSame = account.getNumber() == newAccount.getNumber();
			if(isSame){
				newAccount.setNumber(randomGenerator.nextInt(1000000000));
			}
		}
				
		em.persist(newAccount);
		em.getTransaction().commit();

		em.close();
		return newAccount;
	}

	public static Account GetAccountById(int id) {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		Account AccountbyId = em.find(Account.class, id);

		em.close();
		return AccountbyId;
	}

	public static List<Account> GetAccounts() {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		TypedQuery<Account> tQuery = em.createQuery("from Account", Account.class);
		List<Account> Accounts = tQuery.getResultList();

		em.close();
		return Accounts;
	}

	public static Client loginClient(String username, String passwd) {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();
		Client client = null;
		try {
			TypedQuery<Client> tQuery = em.createQuery(
					"from Client where username = '" + username + "' AND passwd ='" + passwd + "'", Client.class);
			client = tQuery.getSingleResult();
		} catch (NoResultException NoRe) {
			// TODO: handle exception
			client = null;
		}
		em.close();
		return client;
	}

	public static double GetBalance(int accountId) {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		Account account = GetAccountById(accountId);

		List<Transaction> transactions = account.getTransactions();

		double balance = 0;
		for (Transaction t : transactions) {
			balance += t.getAmount();
		}

		em.close();
		return balance;
	}

	public static double GetBalanceAvailable(int clientId) {

		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		TypedQuery<Account> tQuery = em.createQuery("from Account where clientID =" + clientId + "", Account.class);
		List<Account> Accounts = tQuery.getResultList();

		double BalanceAvailable = 0;
		for (Account account : Accounts) {

			int accountId = account.getAccountId();
			double accountBalance = GetBalance(accountId);

			BalanceAvailable += accountBalance;
		}
		em.close();
		return BalanceAvailable;
	}

	public static List<Account> GetAccountsWithoutUserAccount(Account accountSenderId) {
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();

		TypedQuery<Account> tQuery = em
				.createQuery("from Account where accountId != " + accountSenderId.getAccountId() + "", Account.class);
		List<Account> Accounts = tQuery.getResultList();

		em.close();
		return Accounts;
	}

	public static Transaction CreateTransfer(int accountSenderId, int accountRecieverId, String wording, double amount) throws Exception {
		// Ouveerture Connexion
		EntityManager em = ManagerFactory.getInstance().createEntityManager();
		em.getTransaction().begin();
		
		Account accountS = GetAccountById(accountSenderId);
		Account accountR = GetAccountById(accountRecieverId);
		double accountSBalance = ClientManager.GetBalance(accountSenderId);
		
		if (accountSenderId == accountRecieverId) {
			throw new Exception("Vous pouvez pas faire un virement sur le même compte émetteur");
		}
		
		if (accountSBalance < amount) {
			throw new Exception("Le montant saisi est supérieur au solde disponible pour ce compte");
		}
		// Debit Emetteur
		Transaction transactionA = new Transaction();
		transactionA.setAccount(accountS);
		transactionA.setWording(wording);
		transactionA.setDate(new Date());
		transactionA.setAmount(amount * -1);
		accountS.setTransactions(new ArrayList<Transaction>());
		accountS.getTransactions().add(transactionA);

		// Credit Recepteur
		Transaction transactionB = new Transaction();
		transactionB.setAccount(accountR);
		transactionB.setWording(wording);
		transactionB.setDate(new Date());
		transactionB.setAmount(amount);
		accountR.setTransactions(new ArrayList<Transaction>());
		accountR.getTransactions().add(transactionB);

		em.persist(transactionA);
		em.persist(transactionB);
		em.getTransaction().commit();

		em.close();

		return transactionA;
	}

}
