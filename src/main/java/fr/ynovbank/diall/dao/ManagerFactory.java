package fr.ynovbank.diall.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ManagerFactory {

	private static String PERSISTENCE_UNIT_NAME = "appBank";
	private static EntityManagerFactory factory;
	
	private ManagerFactory() {
	}

	public static EntityManagerFactory getInstance() {
		if (factory == null) {
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return factory;
	}
}
