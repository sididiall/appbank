package fr.ynovbank.diall.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;

/**
 * Servlet implementation class CreateAccountServlet
 */
@WebServlet(ServletHelper.CREATE_ACCOUNTS_URL)
public class CreateAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAccountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ServletHelper.CREATE_ACCOUNTS);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String pfix = request.getParameter("wording-prefix");
		String sfix = request.getParameter("wording-suffix");
		
		Client client = (Client) request.getSession().getAttribute("clientlogin");
		int clientid = client.getClientId();
		
		String wording = pfix + " " + sfix;
		Account newAccount;
		if (sfix == null || sfix == "") {
			request.setAttribute("error", "Vous devez saisir un libellé pour le champs suffixe du libellé");
			this.getServletContext().getRequestDispatcher(ServletHelper.CREATE_ACCOUNTS).forward(request, response);
		} else {
			try {
				newAccount = ClientManager.CreateAccount(clientid, client, wording);
				request.setAttribute("newAccount", newAccount);
				this.getServletContext().getRequestDispatcher(ServletHelper.CREATE_ACCOUNTS).forward(request, response);
			} catch (Exception e) {
				request.setAttribute("error", e.getMessage());
				this.getServletContext().getRequestDispatcher(ServletHelper.CREATE_ACCOUNTS).forward(request, response);
			}
		}
		
	}

}
