package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;
import fr.ynovbank.diall.model.Transaction;

/**
 * Servlet implementation class TransferServlet
 */
@WebServlet(ServletHelper.TRANSFER_URL)
public class TransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LogManager.getLogger(TransactionServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TransferServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersistenceUtil util = Persistence.getPersistenceUtil();
		
		Client client = (Client) request.getSession().getAttribute("clientlogin");
		Client clientobj = ClientManager.GetClientById(client.getClientId());
		
		logger.info(clientobj.toString());
		logger.debug("is client loaded ? " + util.isLoaded(clientobj));
		
		List<Account> accountCS = clientobj.getAccounts();
		
		logger.info("is account loaded of client sender? " + util.isLoaded(accountCS));
		logger.info("Size: " + accountCS.size());		

		List<Account> accountCR = ClientManager.GetAccounts();
		
		logger.info("is all accounts loaded of client reciever? " + util.isLoaded(accountCR));
		logger.info("Size: " + accountCR.size());
		
		HashMap<Integer, Double> hashbalanceA = new HashMap<Integer, Double>();
		
		for (Account account: accountCS) {
			int accountid = account.getAccountId();
			double balance = ClientManager.GetBalance(accountid);
			hashbalanceA.put(accountid, balance);
		}
		
		// Envoie vers vue liste de comptes du clients connecté
		request.setAttribute("hashbalanceA", hashbalanceA);
		request.setAttribute("accountCS", accountCS);
		request.setAttribute("accountCR", accountCR);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ServletHelper.TRANSFER);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersistenceUtil util = Persistence.getPersistenceUtil();

		String AccountSenderId = request.getParameter("accountCS");
		logger.info((AccountSenderId));
		int AccountSenderIdInt = Integer.parseInt(AccountSenderId);
		
		String AccountRecieverId = request.getParameter("accountCR");
		logger.info((AccountRecieverId));
		int AccountRecieverIdInt = Integer.parseInt(AccountRecieverId);
		
		String wording = request.getParameter("wording");
		logger.info((wording));
		
		String amountstr = request.getParameter("amount");
		logger.info((amountstr));
		
		double amountdouble = 0; 
		Transaction newTransaction;
		
		if(wording == "") {
			request.setAttribute("errorW", "Vous devez saisir un libellé pour la nouvelle transaction");
			this.getServletContext().getRequestDispatcher(ServletHelper.TRANSFER).forward(request, response);
		}
		else if(amountstr == "") {
			request.setAttribute("errorA", "Vous devez saisir un montant pour la nouvelle transaction");
			this.getServletContext().getRequestDispatcher(ServletHelper.TRANSFER).forward(request, response);
		}else {
			amountdouble = Double.parseDouble(amountstr);
			try {
				newTransaction = ClientManager.CreateTransfer(AccountSenderIdInt, AccountRecieverIdInt, wording, amountdouble);
				if (newTransaction != null) {
					request.setAttribute("newTransaction", newTransaction);
					this.getServletContext().getRequestDispatcher(ServletHelper.TRANSFER).forward(request, response);
				}
			} catch (Exception e) {
				request.setAttribute("error", e.getMessage());
				this.getServletContext().getRequestDispatcher(ServletHelper.TRANSFER).forward(request, response);
			}
		}
	}
}
