package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;
import fr.ynovbank.diall.model.Transaction;

/**
 * Servlet implementation class ServletApiRestPost
 */
@WebServlet("/ServletApiRestPost")
public class ServletApiRestPost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletApiRestPost() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
 		String accountIdS = request.getParameter("accountIdE");
		int accountIdSint = Integer.parseInt(accountIdS);
		
		String accountIdR = request.getParameter("accountIdR");
		int accountIdRint = Integer.parseInt(accountIdR);
		
		String wording = request.getParameter("wording");
		
		String amountstr = request.getParameter("amount");
		double amountdouble = Double.parseDouble(amountstr);

		Transaction transaction;
		response.setContentType("application/json");
		Map<String, Boolean> config = new HashMap<>();
		config.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(config);
		
		try {
			transaction = ClientManager.CreateTransfer(accountIdSint, accountIdRint, wording, amountdouble);

			JsonObjectBuilder objectTransactions = Json.createObjectBuilder()
					.add("transactId", transaction.getTransactId())
					.add("wording", transaction.getWording())
					.add("date", new SimpleDateFormat().format(transaction.getDate()))
					.add("amount", transaction.getAmount());
			
			PrintWriter writer = response.getWriter();
			JsonObject jsonObject = objectTransactions.build();
			writerFactory.createWriter(writer).write(jsonObject);
			String jsonString;
			jsonString = writer.toString();
			writer.println(jsonString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			response.getWriter().append(e.getMessage()).append(request.getContextPath());
		}
	}

}
