package fr.ynovbank.diall.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Client;

/**
 * Servlet implementation class ClientServlet
 */
@WebServlet(ServletHelper.CLIENT_URL)
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ServletHelper.CLIENT);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String oldPasswd = request.getParameter("old-passwd");
		String passwd = request.getParameter("passwd");
		String passwdConfirm = request.getParameter("passwd-confirm");
		
		boolean isSame = passwd.equals(passwdConfirm);
		
		String Uri = request.getRequestURI();
		String clientid = Uri.substring(Uri.lastIndexOf("=")+1);
		
		int clientidint = Integer.parseInt(clientid);
		
		String UriTotransform = request.getRequestURI();
		String uriEncode = Uri.substring(Uri.lastIndexOf("/")+1);
		
		boolean success;
		if(isSame) {
			try {
				success = ClientManager.UpdateClientPassword(clientidint, oldPasswd, passwd);
				if (success) {
					request.setAttribute("success", success);
					this.getServletContext().getRequestDispatcher(ServletHelper.CLIENT).forward(request, response);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				request.setAttribute("error", e.getMessage());
				this.getServletContext().getRequestDispatcher(ServletHelper.CLIENT).forward(request, response);
			}
		}else {
			request.setAttribute("errorform", "Les 2 champs ne sont pas indentique");
			this.getServletContext().getRequestDispatcher(ServletHelper.CLIENT).forward(request, response);
		}
	}

}
