package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.util.List;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;
import fr.ynovbank.diall.model.Transaction;

/**
 * Servlet implementation class TransactionServlet
 */
@WebServlet(ServletHelper.TRANSACTIONS_URL)
public class TransactionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LogManager.getLogger(TransactionServlet.class);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransactionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

    	PersistenceUtil util = Persistence.getPersistenceUtil();
		
		String Uri = request.getRequestURI();
		String acountidx = Uri.substring(Uri.lastIndexOf("=")+1);
		
		int acountidxint = Integer.parseInt(acountidx);
		
		Client client = (Client) request.getSession().getAttribute("clientlogin");
		
		logger.info(client.toString());
		logger.debug("is client loaded ? " + util.isLoaded(client));
		
		List<Account> accounts = client.getAccounts();
		logger.info("is client account ? " + util.isLoaded(accounts));
		logger.info("Size: " + accounts.size());
		
		Account accountByIdx = accounts.get(acountidxint);
		logger.info("is accountbyid loaded ? " + util.isLoaded(accountByIdx));
		logger.info(accountByIdx);
		
		List<Transaction> Transactions = accountByIdx.getTransactions();
		
		logger.info("is transactions loaded ? " + util.isLoaded(Transactions));
		logger.info(Transactions);
		
		request.setAttribute("acountidxint", acountidxint);
		request.setAttribute("accountByIdx", accountByIdx);
		request.setAttribute("Transactions", Transactions);
		
		if (Transactions.isEmpty()) {
			String emptyTransact = "Aucune transaction disponible pour ce compte";
			request.setAttribute("emptyTransact", emptyTransact);
			this.getServletContext().getRequestDispatcher(ServletHelper.TRANSACTIONS).forward(request, response);
		} else {
			this.getServletContext().getRequestDispatcher(ServletHelper.TRANSACTIONS).forward(request, response);;
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
