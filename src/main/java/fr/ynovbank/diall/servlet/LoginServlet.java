package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Client;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(ServletHelper.LOGIN_URL)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String SESSION_EXPIRED = "sessionExpired";
	private static Logger logger = LogManager.getLogger(TransactionServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		long timestamp = new Date().getTime();
		Cookie[] cookies = request.getCookies();

		for (Cookie cookie : cookies) {
			if (cookie.getName().equals(SESSION_EXPIRED)) {
				if (timestamp > Long.parseLong(cookie.getValue())) {
					request.setAttribute("ExpiredMessage", "La session à expirer, vous devez vous reconnecter");
				}
			}
		}

		this.getServletContext().getRequestDispatcher(ServletHelper.LOGIN).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PersistenceUtil util = Persistence.getPersistenceUtil();

		String username = request.getParameter("username");
		String passwd = request.getParameter("passwd");

		Client clientlogin = null;
		clientlogin = ClientManager.loginClient(username, passwd);

		logger.info("is client loaded ? " + util.isLoaded(clientlogin));

		String error = "Erreur sur le login ou le mot de passe";

		if (clientlogin == null) {
			request.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher(ServletHelper.LOGIN).forward(request, response);
		} else {
			long cookieDatecreated = request.getSession().getCreationTime()
					+ request.getSession().getMaxInactiveInterval() * 1000;
			String cookieDatecreatedSting = Long.toString(cookieDatecreated);

			Cookie cookie = new Cookie(SESSION_EXPIRED, cookieDatecreatedSting);
			cookie.setMaxAge((request.getSession().getMaxInactiveInterval() * 4));
			response.addCookie(cookie);

			request.getSession().setAttribute("clientlogin", clientlogin);
			response.sendRedirect(ServletHelper.getServletUrl(ServletHelper.ACCOUNTS_URL, request));
		}

	}

}
