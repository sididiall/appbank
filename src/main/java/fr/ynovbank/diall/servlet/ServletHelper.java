package fr.ynovbank.diall.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet implementation class SevletHelper
 */
public class ServletHelper extends HttpServlet {
	
	//Views Constant
	public static final String LOGIN = "/Views/login.jsp";
	public static final String ACCOUNTS = "/Views/accounts.jsp";
	public static final String CREATE_ACCOUNTS = "/Views/createAccount.jsp";
	public static final String TRANSACTIONS = "/Views/transactions.jsp";
	public static final String TRANSFER = "/Views/createTransfer.jsp";
	public static final String CLIENT = "/Views/updateClient.jsp";
	
	//URL Constante
	public static final String LOGIN_URL = "/Login";
	public static final String LOGOUT_URL = "/Logout";
	public static final String ACCOUNTS_URL = "/Accounts";
	public static final String CREATE_ACCOUNTS_URL = "/CreateAccount";
	public static final String TRANSACTIONS_URL = "/Transactions/*";
	public static final String TRANSFER_URL = "/Transfer";
	public static final String CLIENT_URL = "/Client/*";
	
	public static final String TRANSACTION_REST = "/RestPaginate/*";
       
    /**
     * @see HttpServlet#HttpServlet()
     */

	public static String getServletUrl(String servlet, HttpServletRequest request){
		// TODO Auto-generated method stub
		
		return request.getContextPath()+servlet;
	}

}
