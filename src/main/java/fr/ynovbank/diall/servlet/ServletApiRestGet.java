package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;

/**
 * Servlet implementation class ServletApiRest
 */
@WebServlet("/ServletApiRest/*")
public class ServletApiRestGet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LogManager.getLogger(AccountServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletApiRestGet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PersistenceUtil util = Persistence.getPersistenceUtil();
		
		String Uri = request.getRequestURI();
		String clientId = Uri.substring(Uri.lastIndexOf("/")+1);
		
		int clientIdInt = Integer.parseInt(clientId);
		
		logger.info(Uri);
		logger.info(clientId);
		
		Client client = ClientManager.GetClientById(clientIdInt);
		logger.info(client.toString());
		logger.info("is client loaded ? " + util.isLoaded(client));
		logger.info("is clientbyid loaded ? " + util.isLoaded(client.getClientId()));

		List<Account> Accounts = client.getAccounts();

		logger.info("is accounts of client loaded ? " + util.isLoaded(Accounts));
		logger.info("Size: " + Accounts.size());

		response.setContentType("application/json");
		
		Map<String, Boolean> config = new HashMap<>();
		config.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(config);
		
		JsonObjectBuilder objectBuilderclient = Json.createObjectBuilder()
				.add("clientId", client.getClientId())
				.add("clientNumber", client.getClientNumber())
				.add("name", client.getName())
				.add("firstname", client.getFirstname())
				.add("username", client.getUsername())
				.add("passwd", client.getPasswd());
		
		List<JsonObjectBuilder> accountsobjects = new ArrayList<JsonObjectBuilder>();
		
		for(Account account : Accounts) { 
			JsonObjectBuilder objectBuilderaccounts = Json.createObjectBuilder();
			
		    objectBuilderaccounts.add("accountId", account.getAccountId())
		    					 .add("number", account.getNumber())
		    					 .add("wording", account.getWording());
		    
		    accountsobjects.add(objectBuilderaccounts);
		}
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (JsonObjectBuilder jsonAccountstBuilder : accountsobjects) {
			arrayBuilder.add(jsonAccountstBuilder);
		}
		objectBuilderclient.add("acounts", arrayBuilder);
		
		PrintWriter writer = response.getWriter();
		JsonObject jsonObject = objectBuilderclient.build();
		writerFactory.createWriter(writer).write(jsonObject);
		String jsonString;
		jsonString = writer.toString();
		writer.println(jsonString);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
