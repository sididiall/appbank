package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;
import fr.ynovbank.diall.model.Transaction;

/**
 * Servlet implementation class TransactionRestServlet
 */
@WebServlet(ServletHelper.TRANSACTION_REST)
public class TransactionRestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransactionRestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PersistenceUtil util = Persistence.getPersistenceUtil();
		String Uri = request.getRequestURI();
		String acountidx = Uri.substring(Uri.lastIndexOf("=")+1);
		
		int acountidxint = Integer.parseInt(acountidx);
		
		Client client = (Client) request.getSession().getAttribute("clientlogin");
		
		List<Account> accounts = client.getAccounts();
		
		Account accountByIdx = accounts.get(acountidxint);
		List<Transaction> Transactions = accountByIdx.getTransactions();
		
		response.setContentType("application/json");
		
		Map<String, Boolean> config = new HashMap<>();
		config.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(config);
		
		List<JsonObjectBuilder> transactionobject = new ArrayList<JsonObjectBuilder>();
		JsonObjectBuilder objectBuilderTransact = null;
		for(Transaction transact : Transactions) { 
			objectBuilderTransact = Json.createObjectBuilder();
			
			objectBuilderTransact.add("transactId", transact.getTransactId())
		    					 .add("number", transact.getAmount())
		    					 .add("wording", transact.getWording());
		    
		    transactionobject.add(objectBuilderTransact);
		}
		
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (JsonObjectBuilder jsonAccountstBuilder : transactionobject) {
			arrayBuilder.add(jsonAccountstBuilder);
		}
		objectBuilderTransact.add("transaction", arrayBuilder);
		
		PrintWriter writer = response.getWriter();
		JsonObject jsonObject = objectBuilderTransact.build();
		writerFactory.createWriter(writer).write(jsonObject);
		String jsonString;
		jsonString = writer.toString();
		writer.println(jsonString);
	}

}
