	package fr.ynovbank.diall.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.ynovbank.diall.dao.ClientManager;
import fr.ynovbank.diall.model.Account;
import fr.ynovbank.diall.model.Client;

/**
 * Servlet implementation class AccountServlet
 */

@WebServlet(ServletHelper.ACCOUNTS_URL)
public class AccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LogManager.getLogger(AccountServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccountServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		PersistenceUtil util = Persistence.getPersistenceUtil();
		
		Client client = (Client) request.getSession().getAttribute("clientlogin");
		
		Client clientobj = ClientManager.GetClientById(client.getClientId());
		logger.info(client.toString());
		logger.info("is client loaded ? " + util.isLoaded(client));
		logger.info("is client loaded ? " + util.isLoaded(client.getClientId()));

		HashMap<Integer, Double> hashbalanceA = new HashMap<Integer, Double>();
		
		for (Account account: clientobj.getAccounts()) {
			int accountid = account.getAccountId();
			double balance = ClientManager.GetBalance(accountid);
			hashbalanceA.put(accountid, balance);
		}
		
		double balanceAvailable = ClientManager.GetBalanceAvailable(clientobj.getClientId());

		request.setAttribute("clientobj", clientobj);
		request.setAttribute("hashbalanceA", hashbalanceA);
		request.setAttribute("balanceAvailable", balanceAvailable);
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ServletHelper.ACCOUNTS);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
